<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Question;
use App\Part;

class SubjectCode extends Model
{

    public function get_random_subject_code()
    {
        $list_subject_id = SubjectCode::all('id')->toArray();
        $id_subject = rand(1, count($list_subject_id));
        return $id_subject;

    }

    public function get_random_exam()
    {
        $id = $this->get_random_subject_code();
        return $this->get_exam_by_id($id);
    }

    public function get_exam_by_id($id)
    {
        if ($id) {
            $subject = SubjectCode::find($id);
            $list_part = $subject->parts;

            $arr_questions = [];
            foreach ($list_part as $part) {
                $arr_questions[] = $part->questions->toArray();
            }

            $result = $subject->toArray();

            return $result;
        }

        return false;
    }

    public function parts()
    {
        return $this->hasMany('App\Part', 'subject_code_id', 'id');
    }

    public function deleteSubject($id)
    {
        $subject = SubjectCode::find($id);
        $list_part = $subject->parts;
        foreach ($list_part as $part) {
            $deleteQuestion = Question::where('part_id', $part->id)->delete();
        }

        $deletePart = Part::where('subject_code_id', $id);
        $deleteSubject = SubjectCode::destroy($id);

        return $deleteSubject;
    }

    public static function emptySubject($id)
    {
        $subject = SubjectCode::find($id);
        $list_part = $subject->parts;
        foreach ($list_part as $part) {
            $deleteQuestion = Question::where('part_id', $part->id)->delete();
        }

        $deletePart = Part::where('subject_code_id', $id);

        return $deletePart;
    }

}
