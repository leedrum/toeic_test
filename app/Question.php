<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'the_question',
        'correct_answer',
        'part_id',
        'answer_1',
        'answer_2',
        'answer_3',
        'answer_4',
    ];

    public function part()
    {
        return $this->belongsTo('App\Part', 'part_id', 'id');
    }

    public static function saveQuestion($arr)
    {
        Question::create($arr);
    }
}
