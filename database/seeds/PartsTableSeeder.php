<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PartsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <=7; $i++) {
            DB::table('parts')->insert([
                'part' => $i,
                'subject_code_id' => 1,
            ]);
        }

    }
}
