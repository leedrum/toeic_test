<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubjectCodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subject_codes')->insert([
            'title' => Str::random(10),
            'description' => Str::random(20),
            'listening_file' => Str::random(20),
        ]);
    }
}
