<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            'part_id' => rand(1, 21),
            'the_question' => Str::random(15),
            'correct_answer' => rand(1, 4),
            'answer_1' => Str::random(5),
            'answer_2' => Str::random(5),
            'answer_3' => Str::random(5),
            'answer_4' => Str::random(5),
            'attachment' => Str::random(4).'attachment',
        ]);
    }
}
