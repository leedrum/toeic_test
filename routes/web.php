<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/exams', 'ExamController');

Route::resource('/users', 'UserController');

Route::get('/doExam/{id}', 'ExamController@doExam')->name('doExam');

Route::resource('/subjects', 'SubjectCodeController');

Route::resource('/questions', 'QuestionController');

Route::get('/importQuestionForm', 'ExamController@importQuestionForm')->name('importQuestionForm');

Route::get('/importCalculateGradeForm', function(){
    return view('importCalculateGrade');
})->name('importCalculateGradeForm');

Route::post('/importQuestionExcel', 'ExamController@importQuestionExcel')->name('importQuestionExcel');

Route::post('/importCalculateGradeExcel', 'ExamController@importCalculateGradeForm')->name('importCalculateGradeExcel');

Route::post('/submitExam', 'ExamController@submitExam')->name('submitExam');

Route::get('admin', function(){
    return view('admin-area');
})->name('admin-area');

Route::get('highScore', 'ExamController@highScore')->name('highScore');

Route::get('question/remove/attachment/{id}', 'QuestionController@removeAttachment')->name('remove_attachment');

Route::get('addUser', 'UserController@create')->name('addUser');
