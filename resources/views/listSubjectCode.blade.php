@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row list-button">
            <button class="btn btn-primary show_form_add_subject_code">Add Subject</button>
        </div>
        <br>
        <div class="row form_add_subject_code">
            <form action="{{ route('subjects.store') }}" style="display: none;" enctype="multipart/form-data" class="w-100" method="POST">
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" id="description" cols="30" rows="5" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="listening_file">Listening file</label>
                    <input type="file" class="form-control-file" id="listening_file" name="listening_file">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>

        <br>

        <div class="row">
            @if (\Session::has('error'))
                <div class="alert alert-danger">
                    <ul>
                        <li>{!! \Session::get('error') !!}</li>
                    </ul>
                </div>
            @endif

            @if (\Session::has('msg'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('msg') !!}</li>
                    </ul>
                </div>
            @endif
        </div>

        <div class="row">
            <h1>List Subjects</h1>
            <table class="table table-bordered dataTable" id="datatable_paginate">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Title</th>
                    <th scope="col">Listening file</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($listSubject as $subject)
                    <tr>
                        <th scope="row">{{ $subject->id }}</th>
                        <td>{{ $subject->title }}</td>
                        <td><a href="{{ asset($subject->listening_file) }}" title="click to download">{{ $subject->listening_file }}</a></td>
                        <td>
                            <a href="{{ route('subjects.edit', ['subject' => $subject->id]) }}">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a href="javascript:void(0);" data-subject-id="{{ $subject->id }}" class="delete_subject">
                                <i class="fa fa-trash"></i>
                            </a>
                            <form action="{{ route('subjects.destroy', ['subject' => $subject->id]) }}" class="hidden form_delete_{{ $subject->id }}" method="POST">
                                @method('DELETE')
                                @csrf
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>


    <script>

        jQuery(document).ready(function(){
            jQuery(document).on('click', '.show_form_add_subject_code', function(){
                console.log('ok');
                const form_add_subject = jQuery('.form_add_subject_code form');
                if (form_add_subject.hasClass('show_form')) {
                    form_add_subject.slideUp();
                    form_add_subject.removeClass('show_form');
                } else {
                    form_add_subject.slideDown();
                    form_add_subject.addClass('show_form');
                }

            });

            jQuery(document).on('click', '.delete_subject', function(){
                if (!confirm('Are you sure ?')) {

                    return false;
                } else {

                    let class_form_delete = '.form_delete_' + jQuery(this).attr('data-subject-id');

                    jQuery(class_form_delete).submit();
                }
            });
        });

    </script>
    <link rel="stylesheet" href="{{ asset('lib_admin/datatables/dataTables.bootstrap4.min.css') }}">
    <script src="{{ asset('lib_admin/datatables/jquery.dataTables.min.js') }}" defer></script>
    <script src="{{ asset('lib_admin/datatables/dataTables.bootstrap4.min.js') }}" defer></script>
    <script>
        $(document).ready(function() {
            $('#datatable_paginate').DataTable();
        });
    </script>
@endsection
