@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <h1>Edit User</h1>
        </div>
        <div class="row">
            <a href="{{ route('users.index') }}" class="btn btn-primary  ">Go to list users</a>
        </div>
        <div class="row">
            @if (\Session::has('error'))
                <div class="alert alert-danger">
                    <ul>
                        <li>{!! \Session::get('error') !!}</li>
                    </ul>
                </div>
            @endif

            @if (\Session::has('msg'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('msg') !!}</li>
                    </ul>
                </div>
            @endif
        </div>
        <div class="row">
            <form action="{{ route('users.update', ['subject' => $user->id]) }}" method="POST">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
                </div>
                <div class="form-group">
                    <label for="role">Role</label>
                    <select name="role" id="role" class="form-control">
                        <option value="admin" {{ $user->role == 'admin' ? 'selected' : '' }}>admin</option>
                        <option value="member" {{ $user->role == 'member' ? 'selected' : '' }}>member</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

@endsection
