@extends('layouts.app')
@section('content')


    <div class="container info-exam">
        <div class="row">
            <div class="col-12">
                <h1>Title: {{ $random_exam['title'] }}</h1>
                <div class="description">Description: {{ $random_exam['description'] }}</div>
            </div>
            <div class="col-md-12">
                <p>Đây là bài Thi thử đánh giá năng lực TOEIC hiện tại của bạn ở mức nào.</p>

                <p>Vui lòng lưu ý đây là bài thi mô phỏng đề thi thực tế TOEIC về số câu, cấu trúc đề thi, điểm quy đổi.</p>

                <p>1. Thời gian làm bài 120 phút đếm ngược.</p>

                <p>2. Không bỏ sót câu nào.</p>

                <p>3. Vui lòng điền đầy đủ thông tin để SEE English có cơ sở phân tích, đánh giá bài làm cho bạn và thông báo điểm số cho bạn.</p>

                <p>Chúc bạn làm bài tốt ^^</p>
            </div>
        </div>
    </div>

    <div class="container">
        <button class="btn btn-primary start_exam">Start</button>
    </div>

    <div class="container content-exam d-none">
        <div class="row audio_listening-area">
            <audio src="{{ $random_exam['listening_file'] }}" id="listening_audio"></audio>
        </div>

        @foreach($random_exam['parts'] as $part)
            <h3 class="part-title">Part {{ $part['part'] }}</h3>
            @foreach($part['questions'] as $question)
                <hr>
                <div class="row question-row">
                    <div class="col-12">
                        <div class="row attachment">
						@if(!empty($question['attachment']))
                            <img src="{{ asset($question['attachment']) }}" alt="">
						@endif
                        </div>
                        <div class="row question">
                            <div class="col-12">{{ $question['the_question'] }}</div>
                        </div>

                        <div class="row list-answers-wrap">
                            <div class="col-12 list-answers">
                                <div class="wrap-question">
                                    <input type="radio" name="question_{{ $question['id'] }}" id="question_{{ $question['id'] }}_1" value="1">
                                    <label for="question_{{ $question['id'] }}_1">{{ $question['answer_1'] }}</label>
                                    &nbsp; @if($part['part'] != 1 && $part['part'] != 2) <br> @endif
                                </div>

                                <div class="wrap-question">
                                    <input type="radio" name="question_{{ $question['id'] }}" id="question_{{ $question['id'] }}_2" value="2">
                                    <label for="question_{{ $question['id'] }}_2">{{ $question['answer_2'] }}</label>
                                    &nbsp;@if($part['part'] != 1 && $part['part'] != 2) <br> @endif
                                </div>


                                <div class="wrap-question">
                                    <input type="radio" name="question_{{ $question['id'] }}" id="question_{{ $question['id'] }}_3" value="3">
                                    <label for="question_{{ $question['id'] }}_3">{{ $question['answer_3'] }}</label>
                                    &nbsp; @if($part['part'] != 1 && $part['part'] != 2) <br> @endif
                                </div>
                                <div class="wrap-question">
                                    <input type="radio" name="question_{{ $question['id'] }}" id="question_{{ $question['id'] }}_4" value="4">
                                    <label for="question_{{ $question['id'] }}_4">{{ $question['answer_4'] }}</label>
                                    &nbsp; @if($part['part'] != 1 && $part['part'] != 2) <br> @endif
                                </div>

                                <input type="hidden" value="{{ $question['correct_answer'] }}" class="correct_answer" id="question_{{ $question['id'] }}" data-part="{{ $part['part'] }}">
                                <input type="hidden" id="selected_question_{{ $question['id'] }}" value="0">
                            </div>
                        </div>

                    </div>
                </div>

            @endforeach
        @endforeach

    </div>
    <div class="count-down-area" style="position:fixed; left: 5px;bottom: 5px;border: 2px solid #ccc;padding: 20px;">
        <p id="count-down">Time left 120:00</p>
        <input type="hidden" id="count-down-second" value="7200">
        <input type="hidden" id="listening_correct_count" value="0">
        <input type="hidden" id="reading_correct_count" value="0">
        <button class="btn badge-success" id="submit-exam">Submit Exam</button>
        @if (auth()->check())
        <input type="hidden" name="user_id" id="user_id" value="{{ auth()->user()->id }}">
        <input type="hidden" name="subject_code_id" id="subject_code_id" value="{{ $random_exam['id'] }}">
        @endif
    </div>

    <button type="button" class="btn btn-primary d-none" id="result-grade" data-toggle="modal" data-target="#myModal">Open Modal</button>

    <!-- The Modal -->
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Result of your test</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <p>Listening: <span id="listening_grade"></span></p>
                    <p>Reading: <span id="reading_grade"></span></p>
                    <p>Total: <span id="total_grade"></span></p>
                    <p class="text-center"><a href="{{ route('highScore') }}">Top high score</a></p>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

    <style>

        .hidden-count-down #count-down {display: none;}

    </style>

    <script>
        var count_down_time_left = null;
        jQuery(document).ready(function(){

            jQuery(document).on('click', '.start_exam', function(){

                jQuery(this).addClass('d-none');
                jQuery('.content-exam').removeClass('d-none');

                jQuery(document).on('click', '.list-answers input[type="radio"]', function(){

                    let selected_answer = jQuery(this).attr('value');
                    let id_correct_answer = "#" + jQuery(this).attr('name');
                    let id_selected_question = "#selected_" + jQuery(this).attr('name');
                    let selected_question = parseInt(jQuery(id_selected_question).attr('value'));
                    let correct_answer = jQuery(id_correct_answer).attr('value');
                    let part = jQuery(id_correct_answer).attr('data-part');

                    if (selected_answer === correct_answer) {
                        if (part <= 4) { // <=4 : listening

                            let listening_correct_count_selector = jQuery('#listening_correct_count');
                            let listening_correct_count = listening_correct_count_selector.attr('value');
                            listening_correct_count ++;
                            listening_correct_count_selector.attr('value', listening_correct_count);
                        } else { // reading

                            let reading_correct_count_selector = jQuery('#reading_correct_count');
                            let reading_correct_count = reading_correct_count_selector.attr('value');
                            reading_correct_count ++;
                            reading_correct_count_selector.attr('value', reading_correct_count);
                        }

                        jQuery(id_selected_question).attr('value', 1);
                    } else {

                        jQuery(id_selected_question).attr('value', 0);

                        if (selected_question === 1) {

                            if (part <= 4) { // <=4 : listening

                                let listening_correct_count_selector = jQuery('#listening_correct_count');
                                let listening_correct_count = listening_correct_count_selector.attr('value');
                                listening_correct_count --;
                                listening_correct_count_selector.attr('value', listening_correct_count);
                            } else { // reading

                                let reading_correct_count_selector = jQuery('#reading_correct_count');
                                let reading_correct_count = reading_correct_count_selector.attr('value');
                                reading_correct_count --;
                                reading_correct_count_selector.attr('value', reading_correct_count);
                            }
                        }

                    }

                });


                count_down_time_left = setInterval(function (){
                    start_count_down();
                }, 1000);

                document.getElementById("listening_audio").play();
            });


        });

        function start_count_down() {

            let time = document.getElementById("count-down-second").value;
            if (parseInt(time) === 0) {
                submitExam();
                return false;
            }

            time --;
            let minus = parseInt(time/60);
            let second = time - (minus*60);

            document.getElementById("count-down").innerHTML = "Time left: " + minus + ' : ' + second;
            document.getElementById("count-down-second").value = time;
        }


        function submitExam() {
            let listening_correct_count = jQuery('#listening_correct_count').attr('value');
            let reading_correct_count = jQuery('#reading_correct_count').attr('value');
            clearInterval(count_down_time_left);
            jQuery('#listening_audio').stop();
            const listenning_audio = document.querySelector('#listening_audio');
            listenning_audio.pause();
            listenning_audio.currentTime = 0;
            $.ajax({
                url: "{{ route('submitExam') }}",
                type: 'POST',
                dataType: 'json',
                data: {
                    user_id: jQuery('#user_id').val(),
                    subject_code_id: jQuery('#subject_code_id').val(),
                    listening_correct_count: listening_correct_count,
                    reading_correct_count: reading_correct_count,
                    _token: "{{ csrf_token() }}"
                },
                success: function(result){

                    jQuery('#listening_grade').html(result.grade_listening);
                    jQuery('#reading_grade').html(result.grade_reading);
                    jQuery('#total_grade').html(result.grade_total);

                    jQuery('.count-down-area').css('display', 'none');
                    jQuery('.content-exam').addClass('d-none');

                    jQuery('#result-grade').click(); // open modal result
                }
            });
        }


        jQuery(document).ready(function(){
            jQuery(document).on('click', '#submit-exam', function(){
                submitExam();
            });
        });



    </script>
@endsection
