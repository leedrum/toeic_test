@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>List Exam</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Title</th>
                        <th scope="col">Created at</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($subjects as $subject)
                        <tr>
                            <td scope="col">{{ $subject->id }}</td>
                            <td>{{ $subject->title }}</td>
                            <td>{{ $subject->created_at }}</td>
                            <td>
                                <a href="{{ route('doExam', ['id' => $subject->id]) }}" class="btn btn-success">Do Exam</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <p>Đây là bài Thi thử đánh giá năng lực TOEIC hiện tại của bạn ở mức nào.</p>

                <p>Vui lòng lưu ý đây là bài thi mô phỏng đề thi thực tế TOEIC về số câu, cấu trúc đề thi, điểm quy đổi.</p>

                <p>1. Thời gian làm bài 120 phút đếm ngược.</p>

                <p>2. Không bỏ sót câu nào.</p>

                <p>3. Vui lòng điền đầy đủ thông tin để SEE English có cơ sở phân tích, đánh giá bài làm cho bạn và thông báo điểm số cho bạn.</p>

                <p>Chúc bạn làm bài tốt ^^</p>
            </div>
        </div>
    </div>
@endsection
