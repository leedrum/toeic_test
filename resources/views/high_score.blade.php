@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>High score (top 10)</h1>
                <table class="table">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Listening correct - grade</th>
                        <th>Reading correct - grade</th>
                        <th>Total</th>
                        <th>Time create</th>
                    </tr>
                    <?php $count = 1 ?>
                    @foreach ($top_high_score as $high_score)
                    <tr>
                        <td>{{ $count++ }}</td>
                        <td>{{ $high_score->user->name }}</td>
                        @if ($high_score->listening_correct == 0)
                            <td>0 - 0</td>
                        @else
                            <td>{{ $high_score->listening_correct }} - {{ (\App\Grade::where('total_correct', '=', $high_score->listening_correct)->get())[0]->grade_listening }}</td>
                        @endif

                        @if ($high_score->reading_correct == 0)
                            <td>0 - 0</td>
                        @else
                            <td>{{ $high_score->reading_correct }} - {{ (\App\Grade::where('total_correct', '=', $high_score->reading_correct)->get())[0]->grade_reading }}</td>
                        @endif
                        <td>{{ $high_score->total_grade }}</td>
                        <td>{{ $high_score->created_at }}</td>
                    </tr>
                    @endforeach
                </table>
            </div>

        </div>
    </div>
@endsection
