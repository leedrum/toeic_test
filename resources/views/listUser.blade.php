@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            @if (\Session::has('error'))
                <div class="alert alert-danger">
                    <ul>
                        <li>{!! \Session::get('error') !!}</li>
                    </ul>
                </div>
            @endif

            @if (\Session::has('msg'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('msg') !!}</li>
                    </ul>
                </div>
            @endif
        </div>
        <div class="row">
            <h1 class="w-100">List Users</h1>
            <p><a href="{{ route('addUser') }}" class="btn btn-success">Add User</a></p>
            <table class="table table-bordered dataTable" id="datatable_paginate">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Created at</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td>
                            <a class="" href="{{ route('users.edit', ['users' => $user->id]) }}" style="padding-right: 10px;">
                                <i class="fa fa-pencil"></i>
                            </a>
                            @if (auth()->user()->id != $user->id)
                            <a href="javascript:void(0);" data-user-id="{{ $user->id }}" class="delete_user">
                                <i class="fa fa-trash"></i>
                            </a>
                            <form action="{{ route('users.destroy', ['user' => $user->id]) }}" class="hidden form_delete_{{ $user->id }}" method="POST">
                                @method('DELETE')
                                @csrf
                            </form>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <link rel="stylesheet" href="{{ asset('lib_admin/datatables/dataTables.bootstrap4.min.css') }}">
    <script src="{{ asset('lib_admin/datatables/jquery.dataTables.min.js') }}" defer></script>
    <script src="{{ asset('lib_admin/datatables/dataTables.bootstrap4.min.js') }}" defer></script>

    <script>
        $(document).ready(function() {
            jQuery(document).on('click', '.delete_user', function(){
                if (!confirm('Are you sure ?')) {

                    return false;
                } else {

                    let class_form_delete = '.form_delete_' + jQuery(this).attr('data-user-id');

                    jQuery(class_form_delete).submit();
                }
            });
            $('#datatable_paginate').DataTable();
        });
    </script>
@endsection
