@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            @if (\Session::has('error'))
                <div class="alert alert-danger">
                    <ul>
                        <li>{!! \Session::get('error') !!}</li>
                    </ul>
                </div>
            @endif

            @if (\Session::has('msg'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('msg') !!}</li>
                    </ul>
                </div>
            @endif
        </div>
        <div class="panel panel-default">

            <div class="panel-heading">

                <h1>Import Grades</h1>

            </div>

            <div class="panel-body">


                <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ route('importCalculateGradeExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">

                    @csrf
                    <div class="form-group">
                        <label for="import_file">File</label>
                        <input type="file" class="form-control-file" id="import_file" name="import_file">
                    </div>

                    <button class="btn btn-primary">Import File</button>

                </form>

            </div>

        </div>

    </div>
@endsection
