@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <h1>Edit Subject</h1>
        </div>
        <div class="row">
            <a href="{{ route('subjects.index') }}" class="btn btn-primary  ">Go to list Subject</a>
        </div>
        <div class="row">
            @if (\Session::has('msg'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('msg') !!}</li>
                    </ul>
                </div>
            @endif
        </div>
        <div class="row">

            <form action="{{ route('subjects.update', ['subject' => $subject->id]) }}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{ $subject->title }}">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" id="description" cols="30" rows="5" class="form-control">{{ $subject->description }}</textarea>
                </div>
                <div class="form-group">
                    <label for="listening_file">Listening file</label>
                    <input type="file" class="form-control-file" id="listening_file" name="listening_file">
                    <small>Current file: {{ $subject->listening_file }}</small>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
