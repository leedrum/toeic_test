@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <h1>Edit Question</h1>
        </div>
		<div class="row">
            @if (\Session::has('msg'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('msg') !!}</li>
                    </ul>
                </div>
            @endif
        </div>
        <div class="row">

            <form action="{{ route('questions.update', ['question' => $question->id]) }}" enctype="multipart/form-data" method="POST" class="w-100">
                @csrf
                @method('PUT')
				@if ($errors->any())

					<div class="alert alert-danger">

						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

						<ul>

							@foreach ($errors->all() as $error)

								<li>{{ $error }}</li>

							@endforeach

						</ul>

					</div>

				@endif



				@if (Session::has('success'))

					<div class="alert alert-success">

						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>

						<p>{{ Session::get('success') }}</p>

					</div>

				@endif
                <div class="form-group">
                    <label for="the_questions">The question</label>
                    <input type="text" class="form-control" id="the_question" name="the_question" value="{{ $question->the_question }}">
                </div>

                <div class="form-group">
                    <label for="correct_answer">Correct Answer</label>
                    <input type="text" class="form-control" id="correct_answer" name="correct_answer" value="{{ $question->correct_answer }}">
                    <small>
                        A: 1; B: 2; C: 3; D: 4;
                    </small>
                </div>

                <div class="form-group">
                    <label for="answer_1">Answer 1</label>
                    <input type="text" class="form-control" id="answer_1" name="answer_1" value="{{ $question->answer_1 }}">
                </div>

                <div class="form-group">
                    <label for="answer_2">Answer 2</label>
                    <input type="text" class="form-control" id="answer_2" name="answer_2" value="{{ $question->answer_2 }}">
                </div>

                <div class="form-group">
                    <label for="answer_3">Answer 3</label>
                    <input type="text" class="form-control" id="answer_3" name="answer_3" value="{{ $question->answer_3 }}">
                </div>

                <div class="form-group">
                    <label for="answer_4">Answer 4</label>
                    <input type="text" class="form-control" id="answer_4" name="answer_4" value="{{ $question->answer_4 }}">
                </div>

                <div class="form-group">
                    <small>Part {{ $question->part->part }}</small>
                </div>

                <div class="form-group">
                    <label for="attachment">Attachment</label>
                    <input type="file" class="form-control-file" id="attachment" name="attachment">
                    <small>Current file: {{ $question->attachment != '' ? $question->attachment : 'No attachment' }}</small>
                    @if ($question->attachment)
                    <p><a href="{{ route('remove_attachment', ['id' => $question->id]) }}" class="btn btn-danger">Remove Image</a></p>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
